# Guide Méthodologique : Mise en Open Source d'un Package


## Introduction
Ce guide méthodologique a été rédigé à la suite de la mise en Open Source du package [Melusine](https://github.com/MAIF/melusine).
Nous souhaitons ainsi partager quelques guidelines sur la meilleure manière de packager un projet et de le passer en Open Source.


## List of tools
* [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/) : to initiate a project template.
* [Sphinx](http://www.sphinx-doc.org/en/master/) : to generate the documentation from the docstrings.
* [Travis](https://travis-ci.org/) : continuous integration tool.
* [Readthedoc](https://docs.readthedocs.io/en/latest/intro/getting-started-with-sphinx.html) : documentation generation tool.
* [PyPI](https://pypi.org/)
* [TestPyPI](https://test.pypi.org/) : to test the upgrades of the package before deploying on PiPY
* [Bumpversion](https://github.com/peritus/bumpversion) : versioning tool.



## Step 1 : Creating a virtual environment and activating it
#### With conda
Create the virtual environment :
```
conda create -n my_env_name python=3.6
```
Activating the environment :
```
source activate my_env_name
```
You should now see `(my_env)` to the far left in your terminal prompt.

When you’re finished with your development, deactivate your virtual environment with `deactivate`.

#### With venv
Create the virtual environment :
```
python3.6 -m venv my_env
```
Activating the environment :
```
source my_env/bin/activate
```


## Step 2 : Name your project
They should also be all lowercase and definitely not have any dashes or other punctuation in them. Underscores are discouraged. When you’re building your package, check that the name is available on GitHub, Google, and PyPI.



## Step 3 : Generate your package with Cookiecutter
[Cookiecutter](https://cookiecutter-pypackage.readthedocs.io/en/latest/tutorial.html#step-2-generate-your-package) creates a Python package project from a Python package project template.

#### Install Cookiecutter
**Make sure your virtual environement is activated**.

Install cookiecutter:
```
pip install -U cookiecutter
```

#### Generate your package
Use cookiecutter, pointing it at the cookiecutter-pypackage repo:
```
cookiecutter https://github.com/audreyr/cookiecutter-pypackage.git
```
You’ll be asked to enter a bunch of values to set the package up. If you don’t know what to enter, stick with the defaults.


## Step 4 : Create a Repository

#### GitHub repository
Go to your GitHub account and create a new repo named `mypackage`, where `mypackage` matches the `[project_slug]` from your answers to running cookiecutter.

**If your virtualenv folder is within your project folder, be sure to add the virtualenv folder name to your .gitignore file**.

You will find one folder named after the `[project_slug]`. Move into this folder, and then setup git to use your GitHub repo and upload the code:
```
cd mypackage
git init .
git add .
git commit -m "Initial skeleton."
git remote add origin git@github.com:myusername/mypackage.git
git push -u origin master
```
Where `myusername` and `mypackage` are adjusted for your username and package name.

You’ll need a ssh key to push the repo. You can Generate a key or Add an existing one.

#### GitLab repository



## Step 5 : Configure and install your Dev Requirements
In the top level of your project directory, there should be a `requirements_dev.txt` file. Often this file is named `requirements.txt`. Calling it requirements_dev.txt highlights that **these packages are only installed by project developers**.

#### Configuring requirements_dev.txt
Setup your dev requirements on `requirements_dev.txt`.

Here is an example :
```
pip==18.1
bumpversion==0.5.3
wheel==0.32.1
watchdog==0.9.0
flake8==3.5.0
tox==3.5.2
coverage==4.5.1
Sphinx==1.8.1
sphinx-bootstrap-theme==0.6.5
twine==1.12.1
pytest==3.8.2
pytest-runner==4.2
numpydoc==0.8.0
```

#### Installing requirements_dev.txt
**Your virtualenv should still be activated**. If it isn’t, activate it now. Install the new project’s local development requirements:
```
pip install -r requirements_dev.txt
```
You’ll want to keep these packages updated as newer versions are released. For now, you can install whatever versions are newest by searching PyPI.




## Step 6 : Configure setup.py
The `setup.py` file is the build script for your package. The `setup` function from Setuptools will build your package for upload to PyPI. Setuptools includes information about your package, your version number, and which other packages are required for users.

Here is an exemple of a `setup.py` file :
```
from setuptools import setup

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['pandas>=0.22.0',
                'scikit-learn>=0.19.0',
                ]

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest', ]

setup(
    author="Name of author",
    author_email='email of author',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="Description of package",
    entry_points={},
    install_requires=requirements,
    license="Apache Software License 2.0",
    long_description=readme,
    long_description_content_type='text/markdown',
    include_package_data=True,
    keywords='package_name',
    name='package_name',
    package_dir={
        'package_name': 'package_name',
        'package_name.subpackage1': 'package_name/subpackage1',
        'package_name.subpackage2': 'package_name/subpackage2',
        'package_name.conf': 'package_name/conf',
        'package_name.data': 'package_name/data'
    },
    packages=['package_name', 'package_name.subpackage1', 'package_name.subpackage2'],
    data_files=[('config', ['package_name/conf/conf.json']),
                ('data', ['package_name/data/data.csv'])],
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/project_name',
    version='1.0.0',
    zip_safe=False,
)
```
* `long_description` is set to the contents of your `README.md` file.
* the `requirements` list specified in `setuptools.setup.install_requires` includes all necessary package dependencies for for your package to work. Unlike the list of packages required for development in `requirements_dev.txt`, this list of packages should be as permissive as possible.
* d





### 2 DDD architecture
The project should be implemented using the [DDD architecture](https://gitlab.com/quantmetry/qmtools/20170509_TaskForceDataEngineering_TemplateCode?nav_source=navbar).



### 3 Continuous integration
Different continuous integration tools can be used depending if the repo is on GitHub or GitLab.
* If GitLab use : [gitlab.ci](https://about.gitlab.com/product/continuous-integration/)
* If GitHub use : [travis.ci](https://travis-ci.org/)

#### Travis CI
Travis CI org is a continuous integration tool used to prevent integration problems. Every commit to the master branch will trigger automated builds of the application.

Login using your Github credentials. It may take a few minutes for Travis CI to load up a list of all your GitHub repos. They will be listed with boxes to the left of the repo name, where the boxes have an X in them, meaning it is not connected to Travis CI.

Add the public repo to your Travis CI account by clicking the X to switch it “on” in the box next to the mypackage repo. Do not try to follow the other instructions, that will be taken care of next.

In your terminal, your virtualenv should still be activated. If it isn’t, activate it now. Run the Travis CLI tool to do your Travis CI setup:
```
travis encrypt --add deploy.password
```  

This will:
* Encrypt your PyPI password in your Travis config.
* Activate automated deployment on PyPI when you push a new tag to master branch.


Configure `.travis.yml` :
```
language: python
python:
- 3.6
install: pip install -U tox-travis
script: tox
deploy:
  provider: pypi
  distributions: sdist bdist_wheel
  user: username
  password:
    secure: key
  on:
    tags: true
    repo: repo_name
    python: 3.6
```

If `tags=True` deploy on PyPI with :
```
git push --tags  
```


#### Gitlab CI



### 4 Versioning with Bumpversion
Bumpversion is a small command line tool to simplify releasing software by updating all version strings in your source code by the correct increment.
To install [Bumpversion](https://github.com/peritus/bumpversion):
```
pip install bumpversion
```  
Pour incrémenter la verion du projet :
```
bumpversion [major|minor|patch]
```


### 5 Documentation Sphinx
Sphinx permet de générer la documentation grâce aux docstrings.


## II Open sourcer un projet


### 1 Architecture du projet
Dans le cadre d'une mise en open source il est préconisé de se livrer à une sérieuse étape de brainstorming pour donner une cohérence forte à l'architecture du package : chaque sous-package doit être construit dans un but précis et doit pouvoir être autonome.


### 2 Intégration continue


### 3 Readthedoc
ReadTheDocs hosts documentation for the open source community. Think of it as Continuous Documentation.

Log into your account at ReadTheDocs . If you don’t have one, create one and log into it.

If you are not at your dashboard, choose the pull-down next to your username in the upper right, and select “My Projects”. Choose the button to Import the repository and follow the directions.

In your GitHub repo, select Settings > Webhooks & Services, turn on the ReadTheDocs service hook.

Now your documentation will get rebuilt when you make documentation changes to your package.

### 4 PyPI et TestPyPI

#### Set up pyup.io
pyup.io is a service that helps you to keep your requirements files up to date. It sends you automated pull requests whenever there’s a new release for one of your dependencies.

To use it, create a new account at pyup.io or log into your existing account.

Click on the green Add Repo button in the top left corner and select the repo you created in Step 3. A popup will ask you whether you want to pin your dependencies. Click on Pin to add the repo.

Once your repo is set up correctly, the pyup.io badge will show your current update status.

#### Release on PyPI
The Python Package Index or PyPI is the official third-party software repository for the Python programming language. Python developers intend it to be a comprehensive catalog of all open source Python packages.

When you are ready, release your package the standard Python way.

See PyPI Help for more information about submitting a package.

Here’s a release checklist you can use: [https://gist.github.com/audreyr/5990987](https://gist.github.com/audreyr/5990987)

## III Releasing an upgrade of your package
Use the commands in the following order :
```
git commit -m "description of commit"
```

Depending on the upgrade :
```
bumpversion [major|minor|patch]
```

To launch the build of the doc and PyPI
```
git push --tags
```

To push the modifications :
```
git push
```
